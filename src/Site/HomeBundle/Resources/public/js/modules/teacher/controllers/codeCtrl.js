define(['../config'], function (config) {
    config.controller('codeCtrl', ['$scope', 'codeinfo',"loaderService",
     function ($scope, codeinfo,loaderService) {
        $scope.code = codeinfo;
        loaderService.page('.teacher-content','.teacher-main','move','left');
    }]);
});