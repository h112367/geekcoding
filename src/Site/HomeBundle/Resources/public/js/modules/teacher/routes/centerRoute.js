define([
    './globalRoute',
    '../controllers/centerCtrl',
    '../services/centerService'
], function (config) {
    config.config(function ($stateProvider) {
        $stateProvider
            .state('teacher_home.center', {
                url: "^"+Routing.generate('site_teacher_template_center'),
                views: {
                    "teacher-content": {
                        templateUrl: Routing.generate('site_teacher_template_center'),
                        controller: 'centerCtrl',
                        resolve: {
                            centerinfo: function (viewService) {
                                return viewService.getData('centerService');
                            }
                        }
                    }
                }
            });
    });
});