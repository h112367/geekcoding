define(['../config'], function (config) {
    config.controller('lessonCtrl', ['$scope', 'lessoninfo',"loaderService",
     function ($scope, lessoninfo,loaderService) {
        $scope.lesson = lessoninfo;
        loaderService.page('.teacher-content','.teacher-main','move','left');
    }]);
});