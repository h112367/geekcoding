define([
    './globalRoute',
    './centerRoute',
    './planRoute',
    './lessonRoute',
    './codeRoute'
], function (config) {
    config.config(function ($urlRouterProvider) {
    });
});