define(function(require, exports, module) {
    var $ = require('jquery');
    require('bootstrap'),
    $hideDropmenu = function hideDropmenu($dom){
        $dom.removeClass('open');
    };
    return {
	    	init: function(){
		    	$("header[role='welcome']").find('h1,p').addClass('animated fadeInLeft').show()
		        .closest("header[role='welcome']").find('.loginform').addClass('animated fadeInRight').show();
			    $('.carousel').carousel({
			        interval: 5000
			    });
			    $(".navbar").find(".dropdown")
			    .hover(function(event) {
			        $(this).children('.dropdown-menu').dropdown('toggle');
			    },function(){
			        $(this).children('.dropdown-menu').removeClass('animated fadeInDownBig');
			        $(this).children('.dropdown-menu').addClass('animated fadeOutUpBig');
			        $nav_dropmenu = $(this);
			        setTimeout("$hideDropmenu.call(this,$nav_dropmenu)",200);
			    }).on('show.bs.dropdown', function () {
			        $(this).children('.dropdown-menu').removeClass('animated fadeOutUpBig');
			        $(this).children('.dropdown-menu').addClass('animated fadeInDownBig');
			    }).find('.redirect_link').click(function(event) {
		            window.location.href = $(this).attr('href');
		        });
		        $('.member-panel').addClass("animated bounceInDown").show();
		    }
		}
});