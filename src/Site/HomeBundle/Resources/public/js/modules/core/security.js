define(['jquery','jquerypluginmodal','jquerypluginuser'], function ($) {
	$(function(){
		$('.navbar-login').usercheckmodal();
	    $('.user-login').usercheckmodal({
	        logincheck: {
	            referer: true
	        }
	    });
	    $(".to_login").loginmodal();
        $(".to_registration").registermodal();
        $.login_handle();
        $.register_handle({tooltip_placement:'right',tooltip_container:'body'});
        $('.to_bindoauth').on('click',function(evt){
	        evt.preventDefault();
	        $(this).closest('.createoauth-panel').
	        removeClass('animated lightSpeedIn').addClass('animated lightSpeedOut').fadeOut(200,function(){
	            $('.bindoauth-panel').removeClass('animated lightSpeedOut').addClass('animated lightSpeedIn').show();
	        });
	    });
	    $('.to_createoauth').on('click',function(evt){
	        evt.preventDefault();
	        $(this).closest('.bindoauth-panel').
	        removeClass('animated lightSpeedIn').addClass('animated lightSpeedOut').fadeOut(200,function(){
	            $('.createoauth-panel').removeClass('animated lightSpeedOut').addClass('animated lightSpeedIn').show();
	        });
	    });
	});
});