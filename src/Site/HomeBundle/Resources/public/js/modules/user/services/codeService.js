define(['../config'], function (config) {
    config.factory('codeService', ['$resource' , function ($resource) {
        return $resource(Routing.generate('api_lesson_lesson_get', {index: 'linuxserver'}), {}, {
            query: {method: 'GET', params: {}, isArray: false}
        });
    }]);
});