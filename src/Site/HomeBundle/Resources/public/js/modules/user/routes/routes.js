define([
    './globalRoute',
    './centerRoute',
    './planRoute',
    './lessonRoute',
    './codeRoute'
], function (config) {
    config.config(function ($urlRouterProvider) {
        // $urlRouterProvider.otherwise(Routing.generate("user_home_center")); 
    });
});