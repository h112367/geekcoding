define([
    './globalRoute',
    '../controllers/codeCtrl',
    '../services/codeService'
], function (config) {
    config.config(function ($stateProvider) {
        $stateProvider
        .state('user_home.code', {
                url: "^"+Routing.generate('site_user_template_code'),
                views: {
                    "user-content": {
                        templateUrl: Routing.generate('site_user_template_code'),
                        controller: 'codeCtrl',
                        resolve: {
                            codeinfo: function (viewService) {
                                return viewService.getData('codeService');
                            }
                        }
                    }
            }
        });
    });
});