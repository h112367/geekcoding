define(['../config','../../core/loader'], function (config,loader) {
    config.factory('loaderService',["$rootScope",function ($rootScope) {
    	var loaderService = {};
        var animatetype = {};
        animatetype.bounceDown = {
            start: function(selector1,selector2){
                if ($(selector1).length < 1) {
                    loader.pageloader.show(selector2);
                } else {
                    $(selector1).addClass('animated bounceOutDown').fadeOut(500, function () {
                        loader.pageloader.show(selector2);
                    });
                }
            },
            end: function(selector1,selector2){setTimeout(loader.pageloader.hide, 500);}
        };
        animatetype.fade = {
            start: function(selector1,selector2){
                if ($(selector1).length < 1) {
                    loader.pageloader.show(selector2);
                } else {
                    $(selector1).addClass('animated fadeOut').fadeOut(500, function () {
                        loader.pageloader.show(selector2);
                    });
                }
            },
            end: function(selector1,selector2){setTimeout(loader.pageloader.hide, 500);}
        };
        animatetype.move = {
            start: function(selector1,selector2,options){
                if(typeof(options) == 'undefined'){
                    var type = options;
                }else{
                    var type = 'left';
                }
                var container = $(selector2);
                var slide = container.find(selector1);
                var nslide = slide.clone(true).html('').hide();
                loader.pageloader.show(nslide,'parent');
                slide.parent().css({
                    'overflow': 'hidden',
                    'position': 'relative'
                });
                switch (type) {
                    case 'none':
                        slide.css('float', 'left');
                        nslide.insertAfter(slide);
                        var animate = {
                            'display': 'none'
                        };
                        break;
                    case 'left':
                        slide.css('float', 'left');
                        nslide.insertAfter(slide);
                        var animate = {
                            'margin-left': '-' + slide.parent().css('width')
                        };
                        break;
                    case 'right':
                        slide.css('float', 'right');
                        nslide.insertBefore(slide);
                        var animate = {
                            'margin-right': '-' + slide.parent().css('width')
                        };
                        break;
                    case 'up':
                        nslide.insertAfter(slide);
                        var animate = {
                            'margin-top': '-' + slide.parent().css('height'),
                            'margin-bottom': '+' + slide.parent().css('height')
                        };
                        break;
                    case 'down':
                        var mdown = slide.parent().css('height');
                        nslide.insertBefore(slide);
                        var animate = {
                            'margin-bottom': '-' + slide.parent().css('height'),
                            'margin-top': '+' + slide.parent().css('height')
                        };
                        break;
                    default:
                        slide.css('float', 'left');
                        nslide.insertAfter(slide);
                        var animate = {
                            'display': 'none'
                        };
                        break;
                }
                slide.animate(animate, 500, function() {
                    nslide.show();
                    slide.remove();
                });
            },
            end: function(selector1,selector2){setTimeout(loader.pageloader.hide, 500);}
        };
    	loaderService.page = function(selector1,selector2,animate,options){
            $rootScope.$on('$stateChangeStart', function() {
        	    $.each(animatetype,function(index,value){
                    if(index == animate){
                        value.start(selector1,selector2,options);
                    }
                });
            });
		    $rootScope.$on('$stateChangeSuccess', function() {
                $.each(animatetype,function(index,value){
                    if(index == animate){
                        value.end(selector1,selector2,options);
                    }
                });
		    });
    	}
        loaderService.animate = animatetype;
        return loaderService;
    }]);
    config.factory('viewService', ['$route', '$q', '$injector', function ($route, $q, $injector) {
        return {
            query: function (servicename) {
                var delay = $q.defer();
                Service = $injector.get(servicename);
                Service.query(function (successData) {
                    delay.resolve(successData);
                }, function (errorData) {
                    delay.reject('Unable to query this');
                });
                return delay.promise;
            },
            get: function (servicename,params) {
                var delay = $q.defer();
                Service = $injector.get(servicename);
                Service.get(params,function (successData) {
                    delay.resolve(successData);
                }, function (errorData) {
                    delay.reject('Unable to query this');
                });
                return delay.promise;
            },
            delay: function ($q, $defer) {
                var delay = $q.defer();
                $defer(delay.resolve, 500);
                return delay.promise;
            }
        }
    }]);
});