define(['../config','../../core/videoplayer'], function (config,videoplayer) {
    config.controller('chapterCtrl', ['$scope',
     function ($scope) {
		var videoplayer = new MediaElementPlayer('video',{
			enableAutosize: true,
			success: function(media, node, player) {
				$('#' + node.id + '-mode').html('mode: ' + media.pluginType);
			}
		});
		var widescreen = false;
		var resizevideo = function(){
			var newWidth = $('#video-wrapper').width();
			var newHeight = $('#video-wrapper').height();
			// $(".mejs-time-rail").remove();
			$('.mejs-container').css('width',newWidth);
			videoplayer.setControlsSize();
			$('.mejs-container').css({
				'height':newHeight+'px',
				'max-height':$('#video-wrapper').css('max-height'),
				'min-height':$('#video-wrapper').css('min-height')
			});
		}
		setInterval(resizevideo,30);
		$(".chapter-listin").click(function(){
			if(!$(".chaptershow").hasClass('off')){
				$(".chaptershow").addClass('off');
			}else{
				$(".chaptershow").removeClass('off');
			}
        });
    }]);
});