define(['../config','jquery','../../core/loader','../services/lessonService'], function (config,$,loader) {
    config.controller('lessonCtrl', ['$scope',"lessonService","viewService","$stateParams",
     function ($scope, lessonService,viewService,$stateParams) {
        var chapterlength = [];
        $scope.datacomplete = 'hide';
        $scope.lesson = viewService.get('lessonService',{index: $stateParams.index});
        $scope.lesson.then(function(lesson){
            for (var i = 0; i < lesson.categories.length; i++) {
                chapterlength.push(lesson.categories[i].chapters.length);
            }
            $scope.datacomplete = 'show';
            loader.pageloader.hide();
        });
        $scope.chapterindex = function(cateindex,index){
            chapterbefore = 0;
                for (var i = 0; i < chapterlength.length; i++) {
                    if(cateindex == i){
                    for (var j = 0; j < i + 1; j++) {
                        if((j - 1) in chapterlength){
                            chapterbefore += chapterlength[j - 1];
                        }
                    }
                    return chapterbefore + index + 1;
                }
            }
            return 0;
        }
        // loaderService.page('.teacher-content','.teacher-main','move','left');
    }]);
});