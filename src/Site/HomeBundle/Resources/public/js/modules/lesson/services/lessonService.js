define(['../config'], function (config) {
    config.factory('lessonService', ['$resource', function ($resource) {
        return $resource(Routing.generate('api_lesson_lesson_get') + '/:index', {index: '@index'}, {
            query: {method: 'GET', params: {}, isArray: false}
        });
    }]);
});