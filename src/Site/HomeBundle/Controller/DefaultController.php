<?php

namespace Site\HomeBundle\Controller;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Site\HomeBundle\Controller\BaseController as BaseController;
/**
 * 前台公共控制器
 * @Route("/")
 */
class DefaultController extends BaseController
{
    /**
     * This the documentation description of your method, it will appear
     * on a specific pane. It will read all the text until the first
     * annotation.
     * @Route("/",name="site_home_homepage")
     * @Template
     * @ApiDoc(
     *  resource=true,
     *  description="This is a description of your API method"
     * )
     */
    public function indexAction()
    {
    	$data = array(
            'types' => $this->getModel()->get('SiteLessonBundle:Type')->getAllTypes(),
            'lessons' => $this->getModel()->get('SiteLessonBundle:Lesson')->getAllByLearn()
     	);
        return $data;
    }
}
