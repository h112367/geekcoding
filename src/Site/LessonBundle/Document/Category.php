<?php 
namespace Site\LessonBundle\Document;
use JMS\Serializer\Annotation as JMS;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @JMS\ExclusionPolicy("none")
 * @MongoDB\Document(collection="lesson_categories",repositoryClass="Site\LessonBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @MongoDB\Id(strategy="INCREMENT")
     */
    protected $id;

    /** 
     * @MongoDB\ReferenceOne(targetDocument="Lesson",inversedBy="categories")
     * @JMS\Exclude
     */
    private $lesson;

    /** 
     * @MongoDB\ReferenceMany(targetDocument="Chapter", mappedBy="category")
     * @JMS\Expose
     * @JMS\Groups({"getlesson"})
     */
    private $chapters;

    /**
     * @MongoDB\String
     * @JMS\Expose
     * @JMS\Groups({"getlesson"})
     */
    protected $name;

    /**
     * @MongoDB\Int
     * @JMS\Expose
     */
    protected $order;


    public function __construct()
    {
        $this->chapters = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lesson
     *
     * @param Site\LessonBundle\Document\Lesson $lesson
     * @return self
     */
    public function setLesson(\Site\LessonBundle\Document\Lesson $lesson)
    {
        $this->lesson = $lesson;
        return $this;
    }

    /**
     * Get lesson
     *
     * @return Site\LessonBundle\Document\Lesson $lesson
     */
    public function getLesson()
    {
        return $this->lesson;
    }

    /**
     * Add chapter
     *
     * @param Site\LessonBundle\Document\Chapter $chapter
     */
    public function addChapter(\Site\LessonBundle\Document\Chapter $chapter)
    {
        $this->chapters[] = $chapter;
    }

    /**
     * Remove chapter
     *
     * @param Site\LessonBundle\Document\Chapter $chapter
     */
    public function removeChapter(\Site\LessonBundle\Document\Chapter $chapter)
    {
        $this->chapters->removeElement($chapter);
    }

    /**
     * Get chapters
     *
     * @return Doctrine\Common\Collections\Collection $chapters
     */
    public function getChapters()
    {
        return $this->chapters;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set order
     *
     * @param int $order
     * @return self
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * Get order
     *
     * @return int $order
     */
    public function getOrder()
    {
        return $this->order;
    }

    
}
