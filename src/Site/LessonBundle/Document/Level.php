<?php 
namespace Site\LessonBundle\Document;
use JMS\Serializer\Annotation as JMS;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @JMS\ExclusionPolicy("none")
 * @MongoDB\Document(collection="lesson_levels",repositoryClass="Site\LessonBundle\Repository\LevelRepository")
 */
class Level
{
    /**
     * @MongoDB\Id(strategy="INCREMENT")
     */
    protected $id;

    /** @MongoDB\ReferenceMany(targetDocument="Lesson", mappedBy="level") */
    private $lessons;

    /**
     * @MongoDB\String @MongoDB\UniqueIndex
     * @JMS\Groups({"getlesson"})
     */
    protected $name;

    /**
    * @MongoDB\Int
    */
    protected $order = 0;
    
    public function __construct()
    {
        $this->lessons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add lesson
     *
     * @param Site\LessonBundle\Document\Lesson $lesson
     */
    public function addLesson(\Site\LessonBundle\Document\Lesson $lesson)
    {
        $this->lessons[] = $lesson;
    }

    /**
     * Remove lesson
     *
     * @param Site\LessonBundle\Document\Lesson $lesson
     */
    public function removeLesson(\Site\LessonBundle\Document\Lesson $lesson)
    {
        $this->lessons->removeElement($lesson);
    }

    /**
     * Get lessons
     *
     * @return Doctrine\Common\Collections\Collection $lessons
     */
    public function getLessons()
    {
        return $this->lessons;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set order
     *
     * @param int $order
     * @return self
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * Get order
     *
     * @return int $order
     */
    public function getOrder()
    {
        return $this->order;
    }
}
