<?php
namespace Site\MemberBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AuthListener
{
	private $router;
    private $container;
    private $auth_check;

    public function __construct($router, $container)
    {
        $this->router = $router;
        $this->container = $container;
    }    

    public function onKernelRequest(GetResponseEvent $event)
    {
    	$current_router = $event->getRequest()->get('_route');
    	$is_login = false;
    	if (strstr($current_router, 'member_')) {
            if (($token = $this->container->get('security.context')->getToken()) !== null) {
                if (is_object($user = $token->getUser())) {
		            $is_login = true;
		        }
	        }
	        if($is_login == false){
	        	$url = $this->router->generate('fos_user_security_login');
	        	$event->setResponse(new RedirectResponse($url));
	        }
        }
    }	
}