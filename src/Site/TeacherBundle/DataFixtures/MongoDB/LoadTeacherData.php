<?php 
namespace Site\LessonBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Site\TeacherBundle\Document\Teacher;

class LoadTeacherData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
    	$teachers = array(array('user' => 'lichnow'));
    	$this->insertData($manager,$teachers);
    }
    protected function insertData($manager,$teachers)
    {
    	foreach ($teachers as $key => $value) {
    		$teacher = new Teacher();
    		$teacher->setUser($this->getReference($value['user'].'user'));
    		$manager->persist($teacher);
    		$manager->flush();
            $this->addReference($value['user'].'teacher', $teacher);
    	}
    }
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4;
    }
}