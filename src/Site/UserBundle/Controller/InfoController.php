<?php

namespace Site\UserBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Site\CoreBundle\InterFacer\Controller as BaseController;
/**
 * 前台公共控制器
 */
class InfoController extends BaseController
{
     public function indexAction($step,$type)
     {
          $handle_result = $this->handle($step,$type);
          if($handle_result === true)
          {
               return $this->checkReferer();
          }else{
               return $this->handle($step,$type);
          }
     }
     public function checkReferer()
     {
          if(!$this->getReferer()){
               return $this->redirect($this->generateUrl('site_user_view_center'));
          }else{
               $url = $this->getReferer();
               $this->removeReferer();
               return $this->redirect($url);
          }
     }
     public function handle($step,$type)
     {
          $handle = $this->get('site_user.handle.step');
          $result = $handle->handle($step,$type);
          return $result;
     }
}