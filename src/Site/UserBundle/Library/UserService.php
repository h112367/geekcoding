<?php

namespace Site\UserBundle\Library;
class UserService
{
	public $userlib;
	public function __construct($container)
	{
		$this->userlib = $container->get('tools')->library->get('SiteUserBundle:User');
	}
	public function __call( $method , $args )  
    {  
        return call_user_func_array( array( $this->userlib , $method ) , $args );  
    }  
}