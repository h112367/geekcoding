<?php
namespace Site\CoreBundle\InterFacer;
use Site\CoreBundle\InterFacer\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
abstract class AnnotationController extends Controller{
	public function __construct($container = null)
    {
    	if($container != null)
    		$this->setContainer($container);
    }
}