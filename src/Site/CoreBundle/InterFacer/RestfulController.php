<?php
namespace Site\CoreBundle\InterFacer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Site\CoreBundle\InterFacer\Controller as BaseController;
abstract class RestfulController extends BaseController{
	public function __construct($container = null)
    {
    	if($container != null)
    		$this->setContainer($container);
    }
	public function jsonResult($data){
		return new JsonResponse($data);
	}
}