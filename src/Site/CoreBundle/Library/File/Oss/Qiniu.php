<?php
namespace Site\CoreBundle\Library\File\Oss;
use Site\CoreBundle\InterFacer\Library;
require_once("Qiniu/qiniu/rs.php");
class Qiniu extends Library
{
	public $client;
	public function __construct($container)
	{
		parent::__construct($container);
		$this->setBucket('geekcoding');
		$this->setClient();
	}
	public function setBucket($bucket)
	{
		$this->bucket = $bucket;
	}
	public function setClient($mac = null)
	{
		$this->client = new \Qiniu_MacHttpClient($mac);
	}
	public function getFileProfile($filename)
	{
		return Qiniu_RS_Stat($this->client,$this->bucket,$filename);
	}
	public function moveFile($file1,$file2)
	{
		return Qiniu_RS_Move($this->client, $this->bucket, $file1, $this->bucket, $file2);
	}
}